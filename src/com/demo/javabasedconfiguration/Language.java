package com.demo.javabasedconfiguration;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import javax.security.auth.Destroyable;

/*

 @Author melone
 @Date 6/27/18 
 
 */
public abstract class Language implements InitializingBean, DisposableBean{
    protected abstract void showPosition();

    protected void viewDetail() {
        System.out.println("Programming language");
    }

    /**
     * called when property is set to bean language
     */
    @Override
    public void afterPropertiesSet() {
        //System.out.println("Property set properly");
    }

    /**
     * called just befor bean is going to destroy
     */
    @Override
    public void destroy() {
        //System.out.println("Bean destroyed successfully");

    }

    /**
     * called after initializing the bean is initialized
     * @param o bean object
     * @param s name of bean
     * @return object of bean
     * @throws BeansException
     */
/* implement beanpostprocessor
   @Override
    public Object postProcessAfterInitialization(Object o, String beanname) throws BeansException {
        System.out.println("Bean neme is " + beanname);
        return o;
    }*/

/* used to configure bean from here instead of LanguageApp
implement BeanFactoryPostProcessor
  @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory bean) throws BeansException {

    }*/
}


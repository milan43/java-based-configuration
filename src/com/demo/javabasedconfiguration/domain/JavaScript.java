package com.demo.javabasedconfiguration.domain;

import com.demo.javabasedconfiguration.Language;
import org.springframework.beans.BeansException;

/*

 @Author melone
 @Date 6/27/18 
 
 */
public class JavaScript extends Language {
    @Override
    protected void showPosition() {
        System.out.println("1st position");
    }

    @Override
    protected void viewDetail() {
        System.out.println("Leading the programming");
    }


}

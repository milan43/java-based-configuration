package com.demo.javabasedconfiguration.config;

import com.demo.javabasedconfiguration.Language;
import com.demo.javabasedconfiguration.domain.Java;
import com.demo.javabasedconfiguration.domain.JavaScript;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*

 @Author melone
 @Date 6/27/18 
 
 */
@Configuration
public class LanguageConfig {
    @Bean
    public Language java(){
        return new Java();
    }
    @Bean
    public Language javaScript(){
        return new JavaScript();
    }
    /**
     * we can also wire reference from here as return new Java(javaScript());
    public Language java49(){
        return new Java(49);
    }
     */
}

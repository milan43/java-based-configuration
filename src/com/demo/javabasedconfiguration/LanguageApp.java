package com.demo.javabasedconfiguration;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/*

 @Author melone
 @Date 6/27/18 
 
 */
public class LanguageApp {
    public static void main(String[] args) {
        AbstractApplicationContext context = new FileSystemXmlApplicationContext(
                "src/resources/spring.xml");
        Language language = (Language)context.getBean("java");
        language.showPosition();
        language.viewDetail();
        context.registerShutdownHook();
    }
}
